<?php
class ModelOlapiOrder extends Model {

	public function getOrders($date_begin, $date_end) {
		$sql = "SELECT "
		. "order_id, customer_id, firstname, lastname, email, telephone, payment_firstname, payment_address_1, payment_city, payment_postcode, payment_country, payment_zone, ip, date_added, date_modified" 
		. " FROM `" 
		. DB_PREFIX .  "order` WHERE date_modified >= date('" . $this->db->escape($date_begin) . "') AND date_modified <= date('" . $this->db->escape($date_end) . "')";
		$query = $this->db->query($sql);
		return $query->rows;
	}

}