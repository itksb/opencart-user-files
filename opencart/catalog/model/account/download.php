<?php
class ModelAccountDownload extends Model {
	// запрос проходит при клике на ссылку на скачивание
	public function getDownload($download_id) {
		$implode = array();
	
		$order_statuses = $this->config->get('config_complete_status');

		foreach ($order_statuses as $order_status_id) {
			$implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
		}

		if ($implode) {
			$query = $this->db->query("SELECT d.filename, d.mask FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) LEFT JOIN " . DB_PREFIX . "product_to_download p2d ON (op.product_id = p2d.product_id) LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) WHERE o.customer_id = '" . (int)$this->customer->getId() . "' AND (" . implode(" OR ", $implode) . ") AND d.download_id = '" . (int)$download_id . "'");
			$row = $query->row;	
			if (! $row) {
				$customerCatalogPath = $this->buildCustomerRelativeCatalogPath();
				$filename = self::decodeDownloadId($download_id);
				$fullPath = $customerCatalogPath . DIRECTORY_SEPARATOR . $filename;
				$fullAbsPath = $this->buildCustomerCatalogPath() . DIRECTORY_SEPARATOR . $filename;
				if ($filename && file_exists($fullAbsPath)){
					$row = [
						'filename'=> $fullPath,
						'mask' => 'res_'. $filename	
					];	
				}
			}
			return $row;
		} else {
			return;
		}
	}

	public function getDownloads($start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}

		$implode = array();

		$order_statuses = $this->config->get('config_complete_status');

		foreach ($order_statuses as $order_status_id) {
			$implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
		}

		if ($implode) {
			$query = $this->db->query("SELECT DISTINCT d.download_id, o.order_id, o.date_added, dd.name, d.filename FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) LEFT JOIN " . DB_PREFIX . "product_to_download p2d ON (op.product_id = p2d.product_id) LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE o.customer_id = '" . (int)$this->customer->getId() . "' AND dd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND (" . implode(" OR ", $implode) . ") ORDER BY o.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);
			$rows = $query->rows;
			$rows = is_array($rows) ? $rows : [];
			$rows = array_merge($rows, $this->listCustomerFiles());
			return $rows;
		} else {
			return array();
		}
	}

	/**
	 * Returns current customer Id
	 * @return int
	 */
	protected function getCustomerId(){
		return (int)$this->customer->getId();
	}


	protected function buildCustomerRelativeCatalogPath(){
		$customerId= $this->getCustomerId();
		return 	$customerId . DIRECTORY_SEPARATOR;
	}

	protected function buildCustomerCatalogPath(){
		return 	DIR_DOWNLOAD . $this->buildCustomerRelativeCatalogPath();
	}


	protected function countCustomerFiles(){
		$customerCatalogPath = $this->buildCustomerCatalogPath();
		$customerFiles = [];
		if(file_exists($customerCatalogPath)) {
			$customerFiles = scandir($customerCatalogPath);
			$customerFiles = array_filter($customerFiles, function($filename){
				return ($filename !== '.' && $filename !== '..');
			});
		}
		return count($customerFiles); 
	}

	private function mb_pathinfo($filepath) {
		preg_match('%^(.*?)[\\\\/]*(([^/\\\\]*?)(\.([^\.\\\\/]+?)|))[\\\\/\.]*$%im',$filepath,$m);
		if($m[1]) $ret['dirname']=$m[1];
		if($m[2]) $ret['basename']=$m[2];
		if($m[5]) $ret['extension']=$m[5];
		if($m[3]) $ret['filename']=$m[3];
		return $ret;
	}


	protected static function encodeDownloadId( $str){
		return base64_encode($str);
	}

	protected static function decodeDownloadId( $str){
		return base64_decode($str);
	}

	protected function listCustomerFiles() {
		$customerCatalogPath = $this->buildCustomerCatalogPath();

		$customerFiles = [];

		if(file_exists($customerCatalogPath)) {
			$customerFiles = glob("$customerCatalogPath" . DIRECTORY_SEPARATOR . "*.pdf");
			$customerFiles = array_merge($customerFiles, glob("$customerCatalogPath" . DIRECTORY_SEPARATOR . "*.docx")) ;
			$customerFiles = array_merge($customerFiles, glob("$customerCatalogPath" . DIRECTORY_SEPARATOR . "*.doc")) ;
			$customerFiles = array_merge($customerFiles, glob("$customerCatalogPath" . DIRECTORY_SEPARATOR . "*.xls")) ;
			$customerFiles = array_merge($customerFiles, glob("$customerCatalogPath" . DIRECTORY_SEPARATOR . "*.xlsx")) ;			
			usort($customerFiles, function($a, $b) {
				return filemtime($a) < filemtime($b);
			});
		}

		$resultList = array_map(function($val){
			$pathinfo = $this->mb_pathinfo($val);
			return [
				'download_id' => self::encodeDownloadId ($pathinfo['basename']),
				'order_id' => '#анализ',
				'date_added' => date("Y-m-d H:i:s", filemtime($val)),
				'name' => $pathinfo['basename'] ,
				'filename' => $this->buildCustomerRelativeCatalogPath() . $pathinfo['basename'],
			];	
		}, $customerFiles );

		return $resultList;
	}


	public function getTotalDownloads() {
		$implode = array();
		$order_statuses = $this->config->get('config_complete_status');

		foreach ($order_statuses as $order_status_id) {
			$implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
		}

		if ($implode) {
			$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) LEFT JOIN " . DB_PREFIX . "product_to_download p2d ON (op.product_id = p2d.product_id) WHERE o.customer_id = '" . (int)$this->customer->getId() . "' AND (" . implode(" OR ", $implode) . ")");
			$result = $query->row['total'];
			$result += $this->countCustomerFiles();	
			return (int) $result;
		} else {
			return 0;
		}
	}
}