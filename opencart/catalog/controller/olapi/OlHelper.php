<?php

class OlHelper {

    public static function sessionCheck($db, $api_token, $remote_addr, $session ){
        $result = false;
        $db->query("DELETE FROM `" . DB_PREFIX . "api_session` WHERE TIMESTAMPADD(HOUR, 1, date_modified) < NOW()");
            // Make sure the IP is allowed
        $api_query = $db->query("SELECT DISTINCT * FROM `" 
        . DB_PREFIX . "api` `a` LEFT JOIN `" 
        . DB_PREFIX . "api_session` `as` ON (a.api_id = as.api_id) LEFT JOIN " 
        . DB_PREFIX . "api_ip `ai` ON (a.api_id = ai.api_id) WHERE a.status = '1' AND `as`.`session_id` = '" 
        . $db->escape($api_token)
        . "' AND ai.ip = '" 
        . $db->escape($remote_addr) . "'");
        if ($api_query->num_rows) {
            $session->start($api_token);
            // keep the session alive
            $db->query("UPDATE `" 
            . DB_PREFIX 
            . "api_session` SET `date_modified` = NOW() WHERE `api_session_id` = '" 
            . (int)$api_query->row['api_session_id'] . "'");
            $result = true;
        }

        return $result;
    }


    public static function grabInputJson() {
        $input = file_get_contents('php://input');
     	if ($input === false) {
			return false;
		}

		$assoc = true;
		$json = json_decode( $input, $assoc );

		if (json_last_error() !== JSON_ERROR_NONE) {
			return false;
		}
 
        return  $json;

    }

}