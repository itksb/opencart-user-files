<?php
class ControllerOlapiIndex extends Controller {

    const DOCUMENTATION = <<<documentaion
Документация.
=================================================
Для начала работы с АПИ необходимо авторизоваться. 
Пример запроса:
curl -i -X  POST \
 -F 'username=USERNAME' \
 -F 'key=API_TOKEN' \
http://shop.infomed70.ru/index.php?route=api/login

Либо, используя json-api:

curl -H "Content-Type: application/json" \
  -X POST \
  --data '{"username": "USERNAME", "key":"API_TOKEN"}' \
  http://shop.infomed70.ru/index.php?route=olapi/login
    
Здесь API_TOKEN - token, выданный администратором ресурса,
USERNAME - выданное имя пользователя.

Кроме того, администратор ресурса должен добавить ваш IP в список разрешённых через админ. панель opencart

В ответ сервер отправит api_token, действительный час.

Далее все запросы к АПИ совершаются с использованием токена. 

Запрос списка заказов
=================================================
curl -i -H "Content-Type: application/json" -X POST  \
 -d '{"api_token": "b562ad62674ac020bb1f75c9ca", "date_begin": "2018-09-01", "date_end": "2018-09-29"}'  \
  http://shop.infomed70.ru/index.php?route=olapi/order/orders


documentaion;
    

    public function index(){
        echo self::DOCUMENTATION;
    }

}