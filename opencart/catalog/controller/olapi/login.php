<?php
class ControllerOlapiLogin extends Controller {
	public function index() {
		$this->response->addHeader('Content-Type: application/json');

		$this->load->language('api/login');

		$this->load->model('account/api');

		$input = file_get_contents('php://input');
		if ($input === false) {
			$this->response->setOutput(json_encode('{"error": "no data"}'));
			return ;
		}

		$assoc = true;
		$json = json_decode( $input, $assoc );

		if (json_last_error() !== JSON_ERROR_NONE) {
			$this->response->setOutput(json_encode('{"error": "wrong json format"}'));
			return;
		}

		if (! isset($json['username']) || ! isset($json['key'])) {
			$this->response->setOutput(json_encode('{"error": "username and key fields are required!"}'));
			return;
		}

		$username = $json['username'];
		$key = $json['key'];
		$json = array();		

		// Login with API Key
		$api_info = $this->model_account_api->login($username, $key);

		if ($api_info) {
			// Check if IP is allowed
			$ip_data = array();
	
			$results = $this->model_account_api->getApiIps($api_info['api_id']);
	
			foreach ($results as $result) {
				$ip_data[] = trim($result['ip']);
			}
	
			if (!in_array($this->request->server['REMOTE_ADDR'], $ip_data)) {
				$json['error']['ip'] = sprintf($this->language->get('error_ip'), $this->request->server['REMOTE_ADDR']);
			}				
				
			if (!$json) {
				$json['success'] = $this->language->get('text_success');
				
				$session = new Session($this->config->get('session_engine'), $this->registry);
				$session->start();
				
				$this->model_account_api->addApiSession($api_info['api_id'], $session->getId(), $this->request->server['REMOTE_ADDR']);
				
				$session->data['api_id'] = $api_info['api_id'];
				
				// Create Token
				$json['api_token'] = $session->getId();
			} else {
				$json['error']['key'] = $this->language->get('error_key');
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
	}
}
