<?php

require_once __DIR__ . DIRECTORY_SEPARATOR . 'OlHelper.php';

class ControllerOlapiOrder extends Controller {

	public function orders()
	{
		$json = [];
		$data = OlHelper::grabInputJson();
		$api_token =  isset($data['api_token'])? htmlspecialchars( $data['api_token']) : false;
		$date_begin = isset($data['date_begin']) ?  htmlspecialchars($data['date_begin'] ) : false;
		$date_end = isset($data['date_end']) ? htmlspecialchars($data['date_end']) : false;
		
		if ($api_token && $date_begin && $date_end){
			if (OlHelper::sessionCheck($this->db, $api_token, $_SERVER['REMOTE_ADDR'], $this->session)){
				$this->load->model('olapi/order');
				$json['orders'] = $this->model_olapi_order->getOrders($date_begin, $date_end);
				// SELECT * FROM `oc_order` WHERE date_modified >= date('2018-09-01') AND date_modified <= date('2018-09-22')
			} else {
				$json['error'] = 'error_permission';
			}
		} else {
			$json['error'] = 'fields required: api_token, date_end, date_begin. Ex: {"api_token": "71936daa5b25467922f9c19460", "date_begin": "2018-09-17", "date_end": "2018-09-29"}';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
		
	}

}
